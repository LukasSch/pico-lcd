#include <stdint.h>

#pragma once

const unsigned int DATA_PINS[] = {2, 3, 4, 5, 6, 7, 8, 9};
const unsigned int E_PIN = 15;
const unsigned int RS_PIN = 14;
