#include "lcd.h"

#include <stdint.h>

LCD::LCD(const unsigned int data_pins[8], unsigned int rs_pin, unsigned int e_pin)
{
	int i;

	for (i = 0; i < 8; i++) {
		this->data_pins[i] = data_pins[i];
	}
	this->rs_pin = rs_pin;
	this->e_pin = e_pin;
}

/**
 * \brief Init pins and execute entry mode set instruction.
 *
 * \param interface_length Interface data length. 1: 8 bits. 0: 4 bits.
 * \param line_length Length of the lines.
 * \param lines Number of lines.
 * \param char_font Character font. 1: 5x10 dots. 0: 5x8 dots.
 */
void LCD::init(bool interface_length, uint8_t line_length, uint8_t lines, bool char_font)
{
	int i;
	uint32_t mask;

	for (i = 0; i < 8; i++) {
		mask |= 1 << this->data_pins[i];
	}
	mask |= 1 << this->rs_pin;
	mask |= 1 << this->e_pin;

	gpio_init_mask(mask);
	gpio_set_dir_out_masked(mask);
	gpio_clr_mask(mask);

	/* Make sure everthing is powered up before sending commands. */
	sleep_ms(50);

	this->lines = lines;
	this->line_length = line_length;
	this->function_set_instruction(interface_length, lines > 1, char_font);
}

/**
 * \brief Clear display instruction.
 *
 * \details
 * Clears entire display and sets DDRAM address 0 in address counter.
 */
void LCD::clear_display_instruction(void)
{
	this->set_data_pins(0b00000001);
	this->e_instruction_pulse();
	sleep_ms(3);
}

/**
 * \brief Return home instruction.
 *
 * \details
 * Sets DDRAM address 0 in address counter. Also returns display from being
 * shifted to original position. DDRAM contents remain unchanged.
 */
void LCD::return_home_instruction(void)
{
	this->set_data_pins(0b00000010);
	this->e_instruction_pulse();
	sleep_ms(3);
}

/**
 * \brief Entry mode set instruction.
 *
 * \details
 * Sets cursor move direction and specifies display shift.
 * These operations are performed during data write and read.
 *
 * \param i_d 1: Increment. 0: Decrement.
 * \param s   1: Accompanies display shift
 */
void LCD::entry_mode_set_instruction(bool i_d, bool s)
{
	uint8_t value = 0b00000100;
	value |= i_d << 1;
	value |= s << 0;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief Display on/off control instruction.
 *
 * \details
 * Set entire display on/off, cursor on/off and blinking of cursor position
 * character.
 *
 * \param d Display on/off.
 * \param c Cursor on/off.
 * \param b Cursor blinking.
 */
void LCD::display_control_instruction(bool d, bool c, bool b)
{
	uint8_t value = 0b00001000;
	value |= d << 2;
	value |= c << 1;
	value |= b << 0;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief Cursor or display shift instruction.
 *
 * \details
 * Moves cursor and shifts display without chaning DDRAM contents.
 *
 * \param s_c 1: Display shift. 0: Cursor move.
 * \param r_l 1: Shift to right. 0: Shift to left.
 */
void LCD::cursor_display_shift_instruction(bool s_c, bool r_l)
{
	uint8_t value = 0b00010000;
	value |= s_c << 3;
	value |= r_l << 2;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief Function set instruction.
 *
 * \details
 * Sets interface data length, number of display lines, and character font.
 *
 * \param dl Interface data length. 1: 8 bits. 0: 4 bits.
 * \param n Number of lines. 1: 2 lines. 0: 1 line.
 * \param f Character font. 1: 5x10 dots. 0: 5x8 dots.
 */
void LCD::function_set_instruction(bool dl, bool n, bool f)
{
	uint8_t value = 0b00100000;
	value |= dl << 4;
	value |= n << 3;
	value |= f << 2;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief Set CGRAM address instruction.
 *
 * \details
 * Sets CGRAM address. CGRAM data is sent and received after this setting.
 *
 * \param address
 */
void LCD::set_cgram_address_instruction(uint8_t address)
{
	uint8_t value = address;
	value &= ~(1 << 7); /* Set bit 8 to 0. */
	value |= 1 << 6;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief Set DDRAM address instruction.
 *
 * \details
 * Sets DDRAM address. DDRAM data is sent and received after this setting.
 *
 * \param address
 */
void LCD::set_ddram_address_instruction(uint8_t address)
{
	uint8_t value = address;
	value |= 1 << 7;

	this->set_data_pins(value);
	this->e_instruction_pulse();
	sleep_us(100);
}

/**
 * \brief NOT IMPLEMENTED! Read busy flag & address instruction.
 *
 * \details
 * Reads busy flag indicating internal operation is being performed and reads
 * address counter contents.
 *
 * \param *address Will be set in this fuction.
 *
 * \return busy flag
 */
bool LCD::read_bf_address_instruction(uint8_t *address)
{
	/* NOT IMPLEMENTED */
	return false;
}

/**
 * \brief Write data to CG or DDRAM instruction.
 *
 * \details
 * Writes data into DDRAM or CGRAM.
 *
 * \param data
 */
void LCD::write_data_instruction(uint8_t data)
{
	this->set_data_pins(data);
	this->e_data_pulse();
	sleep_us(100);
}

/**
 * \brief NOT IMPLEMENTED! Read data from CG or DDRAM instruction.
 *
 * \details
 * Reads data from DDRAM or CGRAM.
 *
 * \return data from DDRAM or CGRAM.
 */
uint8_t LCD::read_data_instruction(void)
{
	/* NOT IMPLEMENTED */
	return 0;
}

/**
 * \brief Set rs pin to low and pulse e pin.
 */
void LCD::e_instruction_pulse(void)
{
	gpio_put(this->rs_pin, 0);
	sleep_us(1);
	gpio_put(this->e_pin, 1);
	sleep_us(1);
	gpio_put(this->e_pin, 0);
}

/**
 * \brief Set rs pin to high and pulse e pin.
 */
void LCD::e_data_pulse(void)
{
	gpio_put(this->rs_pin, 1);
	sleep_us(1);
	gpio_put(this->e_pin, 1);
	sleep_us(1);
	gpio_put(this->e_pin, 0);
}

/**
 * \brief Set the output of the data pins to data.
 *
 * \param data
 */
void LCD::set_data_pins(uint8_t data)
{
	int i;
	uint32_t high_mask = 0;
	uint32_t low_mask = 0;

	for (i = 0; i < 8; i++) {
		if (data & 1 << i) {
			high_mask |= 1 << this->data_pins[i];
		} else {
			low_mask |= 1 << this->data_pins[i];
		}
	}

	gpio_set_mask(high_mask);
	gpio_clr_mask(low_mask);
}

/**
 * \brief Controll display using the display controll instruction.
 *
 * \param on_off    Set display on/off.
 * \param cursor    Set cursor on/off.
 * \param blinking  Set cursor blinking on/off.
 */
void LCD::display_control(bool on_off, bool cursor, bool blinking)
{
	this->display_control_instruction(on_off, cursor, blinking);
}

void LCD::set_pos(uint8_t x, uint8_t y)
{
	uint8_t val = x;

	if (y >= 2) {
		val += this->line_length;
	}
	if (y % 2) {
		val |= 1 << 6;
	}

	this->set_ddram_address_instruction(val);

	this->pos_x = x;
	this->pos_y = y;
}

/**
 * \brief Return the locally saved position.
 */
void LCD::get_pos(uint8_t *x, uint8_t *y)
{
	*x = this->pos_x;
	*y = this->pos_y;
}

/**
 * \brief Clear the display using the clear display instruction.
 */
void LCD::clear_display(void)
{
	this->clear_display_instruction();
	this->pos_x = 0;
	this->pos_y = 0;
}

/**
 * \brief Reset position to (0,0) using the return home instruction.
 */
void LCD::return_home(void)
{
	this->return_home_instruction();
	this->pos_x = 0;
	this->pos_y = 0;
}

void LCD::putchar(char c)
{
	bool new_line = false;

	switch (c) {
	case '\n':
	case '\r':
		if (this->pos_x == 0) {
			break;
		}

		new_line = true;
		break;
	case '\t':
		this->write("    ");
		break;
	default:
		this->write_data_instruction(c);
	}

	/* Increment cursor x position. */
	if (new_line) {
		this->pos_x = 0;
	} else {
		this->pos_x++;
	}

	/* Increment cursor y position. */
	this->pos_x %= this->line_length;
	if (this->pos_x == 0) {
		/* Line changed. */
		this->pos_y++;
		this->pos_y %= this->lines;
		this->set_pos(this->pos_x, this->pos_y);
	}
}

void LCD::write(const char *string)
{
	int i;

	for (i = 0; string[i] != '\0'; i++) {
		this->putchar(string[i]);
	}
}
