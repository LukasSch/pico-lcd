#include <stdint.h>

#include "pico/stdlib.h"

class LCD {
private:
	void e_instruction_pulse(void);
	void e_data_pulse(void);
	void set_data_pins(uint8_t data);

	/* HD44780 instructions. */
	void clear_display_instruction(void);
	void return_home_instruction(void);
	void entry_mode_set_instruction(bool i_d, bool s);
	void display_control_instruction(bool d, bool c, bool b);
	void cursor_display_shift_instruction(bool s_c, bool r_l);
	void function_set_instruction(bool dl, bool n, bool f);
	void set_cgram_address_instruction(uint8_t address);
	void set_ddram_address_instruction(uint8_t address);
	bool read_bf_address_instruction(uint8_t *address);
	void write_data_instruction(uint8_t data);
	uint8_t read_data_instruction(void);

	unsigned int data_pins[8];
	unsigned int rs_pin;
	unsigned int e_pin;
	uint8_t lines;
	uint8_t line_length;
	uint8_t pos_x;
	uint8_t pos_y;
public:
	LCD(const unsigned int data_pins[8], unsigned int rs_pin, unsigned int e_pin);

	void init(bool interface_length, uint8_t line_length, uint8_t lines, bool char_font);

	void display_control(bool on_off, bool cursor, bool blinking);
	void clear_display(void);
	void return_home(void);
	void set_pos(uint8_t x, uint8_t y);
	void get_pos(uint8_t *x, uint8_t *y);
	void putchar(char c);
	void write(const char *string);
};
