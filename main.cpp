#include "main.h"

#include <stdint.h>
#include "pico/stdlib.h"
#include "lcd.h"

#ifndef PICO_DEFAULT_LED_PIN
#warning onboard led required
#endif


int
main(void)
{
	stdio_init_all();
	LCD lcd = {DATA_PINS, RS_PIN, E_PIN};
	lcd.init(1, 20, 4, 0);

	lcd.entry_mode_set(1, 0);
	lcd.display_control(1, 1, 1);

	lcd.clear_display();
	lcd.return_home();

	const unsigned int LED_PIN = PICO_DEFAULT_LED_PIN;
	bool led = false;
	gpio_init(LED_PIN);
	gpio_set_dir(LED_PIN, GPIO_OUT);

	lcd.write("Hello World\n");

	while(1) {
		led = !led;
		gpio_put(LED_PIN, led);

		sleep_ms(500);
	}

	return 1;
}
